.class public final Landroid/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final VideoView_error_button:I = 0x1040010

.field public static final VideoView_error_text_invalid_progressive_playback:I = 0x1040015

.field public static final VideoView_error_text_unknown:I = 0x1040011

.field public static final VideoView_error_title:I = 0x1040012

.field public static final cancel:I = 0x1040000

.field public static final copy:I = 0x1040001

.field public static final copyUrl:I = 0x1040002

.field public static final cut:I = 0x1040003

.field public static final defaultMsisdnAlphaTag:I = 0x1040005

.field public static final defaultVoiceMailAlphaTag:I = 0x1040004

.field public static final dialog_alert_title:I = 0x1040014

.field public static final emptyPhoneNumber:I = 0x1040006

.field public static final httpErrorBadUrl:I = 0x1040007

.field public static final httpErrorUnsupportedScheme:I = 0x1040008

.field public static final no:I = 0x1040009

.field public static final ok:I = 0x104000a

.field public static final paste:I = 0x104000b

.field public static final search_go:I = 0x104000c

.field public static final selectAll:I = 0x104000d

.field public static final selectTextMode:I = 0x1040016

.field public static final status_bar_notification_info_overflow:I = 0x1040017

.field public static final uc_alarms_a_starry_night:I = 0x104005d

.field public static final uc_alarms_alarm_clock1:I = 0x104005e

.field public static final uc_alarms_alarm_clock2:I = 0x104005f

.field public static final uc_alarms_clock1:I = 0x1040037

.field public static final uc_alarms_clock2:I = 0x1040038

.field public static final uc_alarms_flyer:I = 0x104003a

.field public static final uc_alarms_jungle:I = 0x1040039

.field public static final uc_alarms_night:I = 0x104003b

.field public static final uc_alarms_rain:I = 0x104003c

.field public static final uc_alarms_spring:I = 0x1040060

.field public static final uc_alarms_walking_in_the_rain:I = 0x1040061

.field public static final uc_default_alarms:I = 0x1040064

.field public static final uc_default_alarms_resource:I = 0x1040067

.field public static final uc_default_notifications:I = 0x1040063

.field public static final uc_default_notifications_resource:I = 0x1040066

.field public static final uc_default_ringtones:I = 0x1040062

.field public static final uc_default_ringtones_resource:I = 0x1040065

.field public static final uc_notifications_carillon:I = 0x1040046

.field public static final uc_notifications_ding:I = 0x1040056

.field public static final uc_notifications_distant:I = 0x1040044

.field public static final uc_notifications_drops:I = 0x1040057

.field public static final uc_notifications_elegance:I = 0x104003d

.field public static final uc_notifications_elegant:I = 0x1040058

.field public static final uc_notifications_free:I = 0x1040041

.field public static final uc_notifications_harp:I = 0x1040059

.field public static final uc_notifications_linger:I = 0x1040040

.field public static final uc_notifications_meet:I = 0x104005a

.field public static final uc_notifications_persistent:I = 0x1040045

.field public static final uc_notifications_petty:I = 0x1040043

.field public static final uc_notifications_place:I = 0x104003e

.field public static final uc_notifications_quickly:I = 0x104005c

.field public static final uc_notifications_surprise:I = 0x104003f

.field public static final uc_notifications_tactfully:I = 0x1040042

.field public static final uc_notifications_wind_chime:I = 0x104005b

.field public static final uc_ringtones_Smart:I = 0x104004b

.field public static final uc_ringtones_beep:I = 0x1040051

.field public static final uc_ringtones_cap:I = 0x104004d

.field public static final uc_ringtones_cloud:I = 0x1040050

.field public static final uc_ringtones_echo:I = 0x1040049

.field public static final uc_ringtones_frost:I = 0x1040047

.field public static final uc_ringtones_journey:I = 0x1040052

.field public static final uc_ringtones_longing:I = 0x1040048

.field public static final uc_ringtones_old:I = 0x104004a

.field public static final uc_ringtones_oneplus_tune:I = 0x1040053

.field public static final uc_ringtones_rotation:I = 0x104004f

.field public static final uc_ringtones_sinnocence:I = 0x1040054

.field public static final uc_ringtones_spirit:I = 0x104004c

.field public static final uc_ringtones_talk:I = 0x104004e

.field public static final uc_ringtones_talk_about:I = 0x1040055

.field public static final unknownName:I = 0x104000e

.field public static final untitled:I = 0x104000f

.field public static final yes:I = 0x1040013


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 14106
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
