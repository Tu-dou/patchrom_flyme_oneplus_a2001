.class public final Lcom/android/internal/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/internal/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final alert_dark_frame:I = 0x1080000

.field public static final alert_light_frame:I = 0x1080001

.field public static final arrow_down_float:I = 0x1080002

.field public static final arrow_up_float:I = 0x1080003

.field public static final bottom_bar:I = 0x108009a

.field public static final btn_check_off:I = 0x10800e6

.field public static final btn_default:I = 0x1080004

.field public static final btn_default_small:I = 0x1080005

.field public static final btn_dialog:I = 0x1080017

.field public static final btn_dropdown:I = 0x1080006

.field public static final btn_minus:I = 0x1080007

.field public static final btn_plus:I = 0x1080008

.field public static final btn_radio:I = 0x1080009

.field public static final btn_star:I = 0x108000a

.field public static final btn_star_big_off:I = 0x108000b

.field public static final btn_star_big_on:I = 0x108000c

.field public static final button_onoff_indicator_off:I = 0x108000e

.field public static final button_onoff_indicator_on:I = 0x108000d

.field public static final call_contact:I = 0x1080277

.field public static final checkbox_off_background:I = 0x108000f

.field public static final checkbox_on_background:I = 0x1080010

.field public static final cling_arrow_up:I = 0x1080278

.field public static final cling_bg:I = 0x1080279

.field public static final cling_button:I = 0x108027a

.field public static final clock_dial:I = 0x108027d

.field public static final clock_hand_hour:I = 0x108027e

.field public static final clock_hand_minute:I = 0x108027f

.field public static final compass_arrow:I = 0x1080285

.field public static final compass_base:I = 0x1080286

.field public static final create_contact:I = 0x1080288

.field public static final dark_header:I = 0x10800a5

.field public static final default_wallpaper:I = 0x108028c

.field public static final demo_test:I = 0x108028d

.field public static final dialog_frame:I = 0x1080011

.field public static final dialog_holo_dark_frame:I = 0x10800b2

.field public static final dialog_holo_light_frame:I = 0x10800b3

.field public static final digital_pen_active:I = 0x10803f2

.field public static final digital_pen_background_side_channel:I = 0x10803f7

.field public static final digital_pen_blocked_mic_red:I = 0x10803f3

.field public static final digital_pen_blocked_mic_yellow:I = 0x10803f4

.field public static final digital_pen_low_battery:I = 0x10803f5

.field public static final digital_pen_positioning_problem:I = 0x10803f6

.field public static final divider_horizontal_bright:I = 0x1080012

.field public static final divider_horizontal_dark:I = 0x1080014

.field public static final divider_horizontal_dim_dark:I = 0x1080015

.field public static final divider_horizontal_textfield:I = 0x1080013

.field public static final edit_text:I = 0x1080016

.field public static final editbox_background:I = 0x1080018

.field public static final editbox_background_normal:I = 0x1080019

.field public static final editbox_dropdown_dark_frame:I = 0x108001a

.field public static final editbox_dropdown_light_frame:I = 0x108001b

.field public static final emo_im_angel:I = 0x10802cf

.field public static final emo_im_cool:I = 0x10802d0

.field public static final emo_im_crying:I = 0x10802d1

.field public static final emo_im_embarrassed:I = 0x10802d2

.field public static final emo_im_foot_in_mouth:I = 0x10802d3

.field public static final emo_im_happy:I = 0x10802d4

.field public static final emo_im_kissing:I = 0x10802d5

.field public static final emo_im_laughing:I = 0x10802d6

.field public static final emo_im_lips_are_sealed:I = 0x10802d7

.field public static final emo_im_money_mouth:I = 0x10802d8

.field public static final emo_im_sad:I = 0x10802d9

.field public static final emo_im_surprised:I = 0x10802da

.field public static final emo_im_tongue_sticking_out:I = 0x10802db

.field public static final emo_im_undecided:I = 0x10802dc

.field public static final emo_im_winking:I = 0x10802dd

.field public static final emo_im_wtf:I = 0x10802de

.field public static final emo_im_yelling:I = 0x10802df

.field public static final emulator_circular_window_overlay:I = 0x10802e0

.field public static final expander_close_holo_dark:I = 0x10802e1

.field public static final expander_ic_maximized:I = 0x10802e8

.field public static final expander_ic_minimized:I = 0x10802e9

.field public static final expander_open_holo_dark:I = 0x10802ea

.field public static final gallery_thumb:I = 0x108001c

.field public static final hy_btn_normal:I = 0x108030e

.field public static final hy_btn_press:I = 0x108030f

.field public static final ic_account_circle:I = 0x1080315

.field public static final ic_audio_alarm:I = 0x1080319

.field public static final ic_audio_alarm_mute:I = 0x108031a

.field public static final ic_audio_bt:I = 0x108031b

.field public static final ic_audio_bt_mute:I = 0x108031d

.field public static final ic_audio_notification:I = 0x108031f

.field public static final ic_audio_notification_mute:I = 0x1080321

.field public static final ic_audio_phone:I = 0x1080323

.field public static final ic_audio_ring_notif:I = 0x1080325

.field public static final ic_audio_ring_notif_mute:I = 0x1080326

.field public static final ic_audio_ring_notif_vibrate:I = 0x1080327

.field public static final ic_audio_vol:I = 0x1080328

.field public static final ic_audio_vol_mute:I = 0x1080329

.field public static final ic_btn_speak_now:I = 0x10800a4

.field public static final ic_bullet_key_permission:I = 0x1080334

.field public static final ic_contact_picture:I = 0x108034c

.field public static final ic_corp_badge:I = 0x1080353

.field public static final ic_corp_icon:I = 0x1080354

.field public static final ic_corp_icon_badge:I = 0x1080355

.field public static final ic_corp_statusbar_icon:I = 0x1080356

.field public static final ic_delete:I = 0x108001d

.field public static final ic_dialog_alert:I = 0x1080027

.field public static final ic_dialog_dialer:I = 0x1080028

.field public static final ic_dialog_email:I = 0x1080029

.field public static final ic_dialog_info:I = 0x108009b

.field public static final ic_dialog_map:I = 0x108002a

.field public static final ic_dialog_usb:I = 0x108035e

.field public static final ic_emergency:I = 0x108035f

.field public static final ic_grayedout_printer:I = 0x108036c

.field public static final ic_input_add:I = 0x108002b

.field public static final ic_input_delete:I = 0x108002c

.field public static final ic_input_get:I = 0x108002d

.field public static final ic_jog_dial_sound_off:I = 0x1080371

.field public static final ic_jog_dial_sound_on:I = 0x1080372

.field public static final ic_jog_dial_unlock:I = 0x1080373

.field public static final ic_jog_dial_vibrate_on:I = 0x1080374

.field public static final ic_lock_airplane_mode:I = 0x1080376

.field public static final ic_lock_airplane_mode_off:I = 0x1080378

.field public static final ic_lock_bugreport:I = 0x108037a

.field public static final ic_lock_idle_alarm:I = 0x108002e

.field public static final ic_lock_idle_charging:I = 0x108001e

.field public static final ic_lock_idle_lock:I = 0x108001f

.field public static final ic_lock_idle_low_battery:I = 0x1080020

.field public static final ic_lock_lock:I = 0x108002f

.field public static final ic_lock_power_off:I = 0x1080030

.field public static final ic_lock_power_reboot:I = 0x1080380

.field public static final ic_lock_silent_mode:I = 0x1080031

.field public static final ic_lock_silent_mode_off:I = 0x1080032

.field public static final ic_maps_indicator_current_position_anim:I = 0x10803ac

.field public static final ic_media_ff:I = 0x1080021

.field public static final ic_media_next:I = 0x1080022

.field public static final ic_media_pause:I = 0x1080023

.field public static final ic_media_play:I = 0x1080024

.field public static final ic_media_previous:I = 0x1080025

.field public static final ic_media_rew:I = 0x1080026

.field public static final ic_media_route_connecting_holo_dark:I = 0x10803b2

.field public static final ic_media_route_disabled_holo_dark:I = 0x10803b5

.field public static final ic_media_route_off_holo_dark:I = 0x10803bb

.field public static final ic_media_route_on_holo_dark:I = 0x10803c7

.field public static final ic_media_stop:I = 0x10803ca

.field public static final ic_media_video_poster:I = 0x10803cb

.field public static final ic_menu_add:I = 0x1080033

.field public static final ic_menu_agenda:I = 0x1080034

.field public static final ic_menu_always_landscape_portrait:I = 0x1080035

.field public static final ic_menu_archive:I = 0x10803ce

.field public static final ic_menu_call:I = 0x1080036

.field public static final ic_menu_camera:I = 0x1080037

.field public static final ic_menu_cc:I = 0x10803d4

.field public static final ic_menu_close_clear_cancel:I = 0x1080038

.field public static final ic_menu_compass:I = 0x1080039

.field public static final ic_menu_crop:I = 0x108003a

.field public static final ic_menu_day:I = 0x108003b

.field public static final ic_menu_delete:I = 0x108003c

.field public static final ic_menu_directions:I = 0x108003d

.field public static final ic_menu_edit:I = 0x108003e

.field public static final ic_menu_gallery:I = 0x108003f

.field public static final ic_menu_goto:I = 0x10803ea

.field public static final ic_menu_help:I = 0x1080040

.field public static final ic_menu_info_details:I = 0x1080041

.field public static final ic_menu_manage:I = 0x1080042

.field public static final ic_menu_mapmode:I = 0x1080043

.field public static final ic_menu_month:I = 0x1080044

.field public static final ic_menu_more:I = 0x1080045

.field public static final ic_menu_moreoverflow_normal_holo_dark:I = 0x10803fc

.field public static final ic_menu_my_calendar:I = 0x1080046

.field public static final ic_menu_mylocation:I = 0x1080047

.field public static final ic_menu_myplaces:I = 0x1080048

.field public static final ic_menu_play_clip:I = 0x1080403

.field public static final ic_menu_preferences:I = 0x1080049

.field public static final ic_menu_recent_history:I = 0x108004a

.field public static final ic_menu_refresh:I = 0x1080404

.field public static final ic_menu_report_image:I = 0x108004b

.field public static final ic_menu_revert:I = 0x108004c

.field public static final ic_menu_rotate:I = 0x108004d

.field public static final ic_menu_save:I = 0x108004e

.field public static final ic_menu_search:I = 0x108004f

.field public static final ic_menu_send:I = 0x1080050

.field public static final ic_menu_set_as:I = 0x1080051

.field public static final ic_menu_share:I = 0x1080052

.field public static final ic_menu_slideshow:I = 0x1080053

.field public static final ic_menu_sort_alphabetically:I = 0x108009c

.field public static final ic_menu_sort_by_size:I = 0x108009d

.field public static final ic_menu_today:I = 0x1080054

.field public static final ic_menu_upload:I = 0x1080055

.field public static final ic_menu_upload_you_tube:I = 0x1080056

.field public static final ic_menu_view:I = 0x1080057

.field public static final ic_menu_week:I = 0x1080058

.field public static final ic_menu_zoom:I = 0x1080059

.field public static final ic_notification_clear_all:I = 0x108005a

.field public static final ic_notification_ime_default:I = 0x1080416

.field public static final ic_notification_overlay:I = 0x108005b

.field public static final ic_partial_secure:I = 0x108005c

.field public static final ic_popup_disk_full:I = 0x108005d

.field public static final ic_popup_reminder:I = 0x108005e

.field public static final ic_popup_sync:I = 0x108005f

.field public static final ic_print:I = 0x108043a

.field public static final ic_print_error:I = 0x108043b

.field public static final ic_search_category_default:I = 0x1080060

.field public static final ic_secure:I = 0x1080061

.field public static final ic_settings:I = 0x1080441

.field public static final ic_settings_language:I = 0x1080442

.field public static final ic_sim_card_multi_24px_clr:I = 0x1080443

.field public static final ic_sim_card_multi_48px_clr:I = 0x1080444

.field public static final ic_text_dot:I = 0x1080448

.field public static final ic_volume:I = 0x108044f

.field public static final indicator_input_error:I = 0x108045a

.field public static final jog_dial_arrow_long_left_green:I = 0x1080461

.field public static final jog_dial_arrow_long_right_red:I = 0x1080464

.field public static final jog_dial_arrow_short_left_and_right:I = 0x1080467

.field public static final jog_dial_bg:I = 0x1080469

.field public static final jog_dial_dimple:I = 0x108046a

.field public static final jog_dial_dimple_dim:I = 0x108046b

.field public static final jog_tab_bar_left_generic:I = 0x1080473

.field public static final jog_tab_bar_left_unlock:I = 0x1080474

.field public static final jog_tab_bar_right_generic:I = 0x108047c

.field public static final jog_tab_bar_right_sound_off:I = 0x108047d

.field public static final jog_tab_bar_right_sound_on:I = 0x108047e

.field public static final jog_tab_left_generic:I = 0x1080484

.field public static final jog_tab_left_unlock:I = 0x1080487

.field public static final jog_tab_right_generic:I = 0x108048d

.field public static final jog_tab_right_sound_off:I = 0x1080490

.field public static final jog_tab_right_sound_on:I = 0x1080491

.field public static final jog_tab_target_gray:I = 0x1080492

.field public static final jog_tab_target_green:I = 0x1080493

.field public static final jog_tab_target_yellow:I = 0x1080495

.field public static final list_selector_background:I = 0x1080062

.field public static final list_selector_pressed_holo_dark:I = 0x10804d5

.field public static final loading_tile_android:I = 0x10804f8

.field public static final magnified_region_frame:I = 0x10804fa

.field public static final maps_google_logo:I = 0x10804fb

.field public static final menu_background:I = 0x10804fd

.field public static final menu_frame:I = 0x1080063

.field public static final menu_full_frame:I = 0x1080064

.field public static final menuitem_background:I = 0x1080065

.field public static final no_tile_256:I = 0x1080518

.field public static final notification_icon_legacy_bg:I = 0x1080519

.field public static final notification_template_icon_bg:I = 0x10807cb

.field public static final notification_template_icon_low_bg:I = 0x10807cc

.field public static final op_default_button_bg:I = 0x1080546

.field public static final op_edit_text_material:I = 0x1080547

.field public static final op_edit_text_material_dark:I = 0x1080548

.field public static final op_letter_photo_a0:I = 0x108ff53

.field public static final op_letter_photo_a1:I = 0x108ff54

.field public static final op_letter_photo_a2:I = 0x108ff55

.field public static final op_letter_photo_a3:I = 0x108ff56

.field public static final op_letter_photo_a4:I = 0x108ff57

.field public static final op_letter_photo_a5:I = 0x108ff58

.field public static final op_letter_photo_a6:I = 0x108ff59

.field public static final op_letter_photo_a7:I = 0x108ff60

.field public static final op_letter_photo_b0:I = 0x108ff61

.field public static final op_letter_photo_b1:I = 0x108ff62

.field public static final op_letter_photo_b2:I = 0x108ff63

.field public static final op_letter_photo_b3:I = 0x108ff64

.field public static final op_letter_photo_b4:I = 0x108ff65

.field public static final op_letter_photo_b5:I = 0x108ff66

.field public static final op_letter_photo_b6:I = 0x108ff67

.field public static final op_letter_photo_b7:I = 0x108ff68

.field public static final op_letter_photo_c0:I = 0x108ff69

.field public static final op_letter_photo_c1:I = 0x108ff6a

.field public static final op_letter_photo_c2:I = 0x108ff6b

.field public static final op_letter_photo_c3:I = 0x108ff6c

.field public static final op_letter_photo_c4:I = 0x108ff6d

.field public static final op_letter_photo_c5:I = 0x108ff6e

.field public static final op_letter_photo_c6:I = 0x108ff6f

.field public static final op_letter_photo_c7:I = 0x108ff70

.field public static final op_letter_photo_d0:I = 0x108ff71

.field public static final op_letter_photo_d1:I = 0x108ff72

.field public static final op_letter_photo_d2:I = 0x108ff73

.field public static final op_letter_photo_d3:I = 0x108ff74

.field public static final op_letter_photo_d4:I = 0x108ff75

.field public static final op_letter_photo_d5:I = 0x108ff76

.field public static final op_letter_photo_d6:I = 0x108ff77

.field public static final op_letter_photo_d7:I = 0x108ff78

.field public static final op_letter_photo_e0:I = 0x108ff79

.field public static final op_letter_photo_e1:I = 0x108ff7a

.field public static final op_letter_photo_e2:I = 0x108ff7b

.field public static final op_letter_photo_e3:I = 0x108ff7c

.field public static final op_letter_photo_e4:I = 0x108ff7d

.field public static final op_letter_photo_e5:I = 0x108ff7e

.field public static final op_letter_photo_e6:I = 0x108ff7f

.field public static final op_letter_photo_e7:I = 0x108ff80

.field public static final op_letter_photo_f0:I = 0x108ff81

.field public static final op_letter_photo_f1:I = 0x108ff82

.field public static final op_letter_photo_f2:I = 0x108ff83

.field public static final op_letter_photo_f3:I = 0x108ff84

.field public static final op_letter_photo_f4:I = 0x108ff85

.field public static final op_letter_photo_f5:I = 0x108ff86

.field public static final op_letter_photo_f6:I = 0x108ff87

.field public static final op_letter_photo_f7:I = 0x108ff88

.field public static final op_letter_photo_g0:I = 0x108ff89

.field public static final op_letter_photo_g1:I = 0x108ff8a

.field public static final op_letter_photo_g2:I = 0x108ff8b

.field public static final op_letter_photo_g3:I = 0x108ff8c

.field public static final op_letter_photo_g4:I = 0x108ff8d

.field public static final op_letter_photo_g5:I = 0x108ff8f

.field public static final op_letter_photo_g6:I = 0x108ff90

.field public static final op_letter_photo_g7:I = 0x108ff91

.field public static final op_letter_photo_h0:I = 0x108ff92

.field public static final op_letter_photo_h1:I = 0x108ff93

.field public static final op_letter_photo_h2:I = 0x108ff94

.field public static final op_letter_photo_h3:I = 0x108ff95

.field public static final op_letter_photo_h4:I = 0x108ff96

.field public static final op_letter_photo_h5:I = 0x108ff97

.field public static final op_letter_photo_h6:I = 0x108ff98

.field public static final op_letter_photo_h7:I = 0x108ff99

.field public static final op_letter_photo_i0:I = 0x108ff9a

.field public static final op_letter_photo_i1:I = 0x108ff9b

.field public static final op_letter_photo_i2:I = 0x108ff9c

.field public static final op_letter_photo_i3:I = 0x108ff9d

.field public static final op_letter_photo_i4:I = 0x108ff9e

.field public static final op_letter_photo_i5:I = 0x108ff9f

.field public static final op_letter_photo_i6:I = 0x108ffa0

.field public static final op_letter_photo_i7:I = 0x108ffa1

.field public static final op_letter_photo_j0:I = 0x108ffa2

.field public static final op_letter_photo_j1:I = 0x108ffa3

.field public static final op_letter_photo_j2:I = 0x108ffa4

.field public static final op_letter_photo_j3:I = 0x108ffa5

.field public static final op_letter_photo_j4:I = 0x108ffa6

.field public static final op_letter_photo_j5:I = 0x108ffa7

.field public static final op_letter_photo_j6:I = 0x108ffa8

.field public static final op_letter_photo_j7:I = 0x108ffa9

.field public static final op_letter_photo_k0:I = 0x108ffaa

.field public static final op_letter_photo_k1:I = 0x108ffab

.field public static final op_letter_photo_k2:I = 0x108ffac

.field public static final op_letter_photo_k3:I = 0x108ffad

.field public static final op_letter_photo_k4:I = 0x108ffae

.field public static final op_letter_photo_k5:I = 0x108ffaf

.field public static final op_letter_photo_k6:I = 0x108ffb0

.field public static final op_letter_photo_k7:I = 0x108ffb1

.field public static final op_letter_photo_l0:I = 0x108ffb2

.field public static final op_letter_photo_l1:I = 0x108ffb3

.field public static final op_letter_photo_l2:I = 0x108ffb4

.field public static final op_letter_photo_l3:I = 0x108ffb5

.field public static final op_letter_photo_l4:I = 0x108ffb6

.field public static final op_letter_photo_l5:I = 0x108ffb7

.field public static final op_letter_photo_l6:I = 0x108ffb8

.field public static final op_letter_photo_l7:I = 0x108ffb9

.field public static final op_letter_photo_m0:I = 0x108ffba

.field public static final op_letter_photo_m1:I = 0x108ffbb

.field public static final op_letter_photo_m2:I = 0x108ffbc

.field public static final op_letter_photo_m3:I = 0x108ffbd

.field public static final op_letter_photo_m4:I = 0x108ffbe

.field public static final op_letter_photo_m5:I = 0x108ffbf

.field public static final op_letter_photo_m6:I = 0x108ffc0

.field public static final op_letter_photo_m7:I = 0x108ffc1

.field public static final op_letter_photo_n0:I = 0x108ffc2

.field public static final op_letter_photo_n1:I = 0x108ffc3

.field public static final op_letter_photo_n2:I = 0x108ffc4

.field public static final op_letter_photo_n3:I = 0x108ffc5

.field public static final op_letter_photo_n4:I = 0x108ffc6

.field public static final op_letter_photo_n5:I = 0x108ffc7

.field public static final op_letter_photo_n6:I = 0x108ffc8

.field public static final op_letter_photo_n7:I = 0x108ffc9

.field public static final op_letter_photo_o0:I = 0x108ffca

.field public static final op_letter_photo_o1:I = 0x108ffcb

.field public static final op_letter_photo_o2:I = 0x108ffcc

.field public static final op_letter_photo_o3:I = 0x108ffcd

.field public static final op_letter_photo_o4:I = 0x108ffce

.field public static final op_letter_photo_o5:I = 0x108ffcf

.field public static final op_letter_photo_o6:I = 0x108ffd0

.field public static final op_letter_photo_o7:I = 0x108ffd1

.field public static final op_letter_photo_p0:I = 0x108ffd2

.field public static final op_letter_photo_p1:I = 0x108ffd3

.field public static final op_letter_photo_p2:I = 0x108ffd4

.field public static final op_letter_photo_p3:I = 0x108ffd5

.field public static final op_letter_photo_p4:I = 0x108ffd6

.field public static final op_letter_photo_p5:I = 0x108ffd7

.field public static final op_letter_photo_p6:I = 0x108ffd8

.field public static final op_letter_photo_p7:I = 0x108ffd9

.field public static final op_letter_photo_q0:I = 0x108ffda

.field public static final op_letter_photo_q1:I = 0x108ffdb

.field public static final op_letter_photo_q2:I = 0x108ffdc

.field public static final op_letter_photo_q3:I = 0x108ffde

.field public static final op_letter_photo_q4:I = 0x108ffdf

.field public static final op_letter_photo_q5:I = 0x108ffe0

.field public static final op_letter_photo_q6:I = 0x108ffe1

.field public static final op_letter_photo_q7:I = 0x108ffe2

.field public static final op_letter_photo_r0:I = 0x108ffe3

.field public static final op_letter_photo_r1:I = 0x108ffe4

.field public static final op_letter_photo_r2:I = 0x108ffe5

.field public static final op_letter_photo_r3:I = 0x108ffe6

.field public static final op_letter_photo_r4:I = 0x108ffe7

.field public static final op_letter_photo_r5:I = 0x108ffe8

.field public static final op_letter_photo_r6:I = 0x108ffe9

.field public static final op_letter_photo_r7:I = 0x108ffea

.field public static final op_letter_photo_s0:I = 0x108ffeb

.field public static final op_letter_photo_s1:I = 0x108ffec

.field public static final op_letter_photo_s2:I = 0x108ffed

.field public static final op_letter_photo_s3:I = 0x108ffee

.field public static final op_letter_photo_s4:I = 0x108ffef

.field public static final op_letter_photo_s5:I = 0x108fff0

.field public static final op_letter_photo_s6:I = 0x108fff1

.field public static final op_letter_photo_s7:I = 0x108fff2

.field public static final op_letter_photo_t0:I = 0x108fff3

.field public static final op_letter_photo_t1:I = 0x108fff4

.field public static final op_letter_photo_t2:I = 0x108fff5

.field public static final op_letter_photo_t3:I = 0x108fff6

.field public static final op_letter_photo_t4:I = 0x108fff7

.field public static final op_letter_photo_t5:I = 0x108fff8

.field public static final op_letter_photo_t6:I = 0x108fff9

.field public static final op_letter_photo_t7:I = 0x108fffa

.field public static final op_letter_photo_u0:I = 0x108fffb

.field public static final op_letter_photo_u1:I = 0x108fffc

.field public static final op_letter_photo_u2:I = 0x108fffd

.field public static final op_letter_photo_u3:I = 0x108fffe

.field public static final op_letter_photo_u4:I = 0x108ff4f

.field public static final op_letter_photo_u5:I = 0x108ff4e

.field public static final op_letter_photo_u6:I = 0x108ff4d

.field public static final op_letter_photo_u7:I = 0x108ff4c

.field public static final op_letter_photo_v0:I = 0x108ff4b

.field public static final op_letter_photo_v1:I = 0x108ff4a

.field public static final op_letter_photo_v2:I = 0x108ff49

.field public static final op_letter_photo_v3:I = 0x108ff48

.field public static final op_letter_photo_v4:I = 0x108ff47

.field public static final op_letter_photo_v5:I = 0x108ff46

.field public static final op_letter_photo_v6:I = 0x108ff45

.field public static final op_letter_photo_v7:I = 0x108ff44

.field public static final op_letter_photo_w0:I = 0x108ff43

.field public static final op_letter_photo_w1:I = 0x108ff42

.field public static final op_letter_photo_w2:I = 0x108ff41

.field public static final op_letter_photo_w3:I = 0x108ff40

.field public static final op_letter_photo_w4:I = 0x108ff3f

.field public static final op_letter_photo_w5:I = 0x108ff3e

.field public static final op_letter_photo_w6:I = 0x108ff3d

.field public static final op_letter_photo_w7:I = 0x108ff3c

.field public static final op_letter_photo_x0:I = 0x108ff3b

.field public static final op_letter_photo_x1:I = 0x108ff3a

.field public static final op_letter_photo_x2:I = 0x108ff39

.field public static final op_letter_photo_x3:I = 0x108ff38

.field public static final op_letter_photo_x4:I = 0x108ff37

.field public static final op_letter_photo_x5:I = 0x108ff36

.field public static final op_letter_photo_x6:I = 0x108ff35

.field public static final op_letter_photo_x7:I = 0x108ff34

.field public static final op_letter_photo_y0:I = 0x108ff33

.field public static final op_letter_photo_y1:I = 0x108ff32

.field public static final op_letter_photo_y2:I = 0x108ff31

.field public static final op_letter_photo_y3:I = 0x108ff30

.field public static final op_letter_photo_y4:I = 0x108ff2f

.field public static final op_letter_photo_y5:I = 0x108ff2e

.field public static final op_letter_photo_y6:I = 0x108ff2d

.field public static final op_letter_photo_y7:I = 0x108ff2c

.field public static final op_letter_photo_z0:I = 0x108ff2b

.field public static final op_letter_photo_z1:I = 0x108ff2a

.field public static final op_letter_photo_z2:I = 0x108ff29

.field public static final op_letter_photo_z3:I = 0x108ff28

.field public static final op_letter_photo_z4:I = 0x108ff27

.field public static final op_letter_photo_z5:I = 0x108ff26

.field public static final op_letter_photo_z6:I = 0x108ff25

.field public static final op_letter_photo_z7:I = 0x108ff24

.field public static final op_me_photo:I = 0x108ff50

.field public static final op_others_photo:I = 0x108ff51

.field public static final op_preference_single_item:I = 0x1080549

.field public static final op_stranger_photo:I = 0x108ff52

.field public static final picture_emergency:I = 0x1080572

.field public static final picture_frame:I = 0x1080066

.field public static final platlogo:I = 0x1080573

.field public static final popup_bottom_bright:I = 0x1080580

.field public static final popup_bottom_dark:I = 0x1080581

.field public static final popup_bottom_medium:I = 0x1080582

.field public static final popup_center_bright:I = 0x1080583

.field public static final popup_center_dark:I = 0x1080584

.field public static final popup_full_bright:I = 0x1080586

.field public static final popup_full_dark:I = 0x1080587

.field public static final popup_top_bright:I = 0x1080594

.field public static final popup_top_dark:I = 0x1080595

.field public static final presence_audio_away:I = 0x10800af

.field public static final presence_audio_busy:I = 0x10800b0

.field public static final presence_audio_online:I = 0x10800b1

.field public static final presence_away:I = 0x1080067

.field public static final presence_busy:I = 0x1080068

.field public static final presence_invisible:I = 0x1080069

.field public static final presence_offline:I = 0x108006a

.field public static final presence_online:I = 0x108006b

.field public static final presence_video_away:I = 0x10800ac

.field public static final presence_video_busy:I = 0x10800ad

.field public static final presence_video_online:I = 0x10800ae

.field public static final progress_horizontal:I = 0x108006c

.field public static final progress_indeterminate_horizontal:I = 0x108006d

.field public static final quickcontact_badge_overlay_dark:I = 0x10805c3

.field public static final radiobutton_off_background:I = 0x108006e

.field public static final radiobutton_on_background:I = 0x108006f

.field public static final reticle:I = 0x10805ff

.field public static final screen_background_dark:I = 0x1080098

.field public static final screen_background_dark_transparent:I = 0x10800a9

.field public static final screen_background_light:I = 0x1080099

.field public static final screen_background_light_transparent:I = 0x10800aa

.field public static final scrubber_control_disabled_holo:I = 0x1080608

.field public static final scrubber_control_selector_holo:I = 0x1080617

.field public static final scrubber_progress_horizontal_holo_dark:I = 0x1080621

.field public static final search_spinner:I = 0x108062d

.field public static final sim_dark_blue:I = 0x1080636

.field public static final sim_dark_green:I = 0x1080637

.field public static final sim_dark_orange:I = 0x1080638

.field public static final sim_dark_purple:I = 0x1080639

.field public static final sim_light_blue:I = 0x108063a

.field public static final sim_light_green:I = 0x108063b

.field public static final sim_light_orange:I = 0x108063c

.field public static final sim_light_purple:I = 0x108063d

.field public static final spinner_background:I = 0x1080070

.field public static final spinner_dropdown_background:I = 0x1080071

.field public static final star_big_off:I = 0x1080073

.field public static final star_big_on:I = 0x1080072

.field public static final star_off:I = 0x1080075

.field public static final star_on:I = 0x1080074

.field public static final stat_notify_call_mute:I = 0x1080076

.field public static final stat_notify_car_mode:I = 0x108067e

.field public static final stat_notify_chat:I = 0x1080077

.field public static final stat_notify_disabled_data:I = 0x108067f

.field public static final stat_notify_disk_full:I = 0x1080680

.field public static final stat_notify_error:I = 0x1080078

.field public static final stat_notify_missed_call:I = 0x108007f

.field public static final stat_notify_more:I = 0x1080079

.field public static final stat_notify_rssi_in_range:I = 0x1080683

.field public static final stat_notify_sdcard:I = 0x108007a

.field public static final stat_notify_sdcard_prepare:I = 0x10800ab

.field public static final stat_notify_sdcard_usb:I = 0x108007b

.field public static final stat_notify_sim_toolkit:I = 0x1080684

.field public static final stat_notify_sync:I = 0x108007c

.field public static final stat_notify_sync_error:I = 0x1080686

.field public static final stat_notify_sync_noanim:I = 0x108007d

.field public static final stat_notify_voicemail:I = 0x108007e

.field public static final stat_notify_wifi_in_range:I = 0x1080687

.field public static final stat_sys_adb:I = 0x1080688

.field public static final stat_sys_battery:I = 0x1080689

.field public static final stat_sys_battery_charge:I = 0x1080697

.field public static final stat_sys_battery_unknown:I = 0x10806a5

.field public static final stat_sys_certificate_info:I = 0x10806a6

.field public static final stat_sys_data_bluetooth:I = 0x1080080

.field public static final stat_sys_data_usb:I = 0x10806a7

.field public static final stat_sys_download:I = 0x1080081

.field public static final stat_sys_download_done:I = 0x1080082

.field public static final stat_sys_gps_on:I = 0x10806b1

.field public static final stat_sys_headset:I = 0x1080083

.field public static final stat_sys_phone_call:I = 0x1080084
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_forward:I = 0x1080085
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_phone_call_on_hold:I = 0x1080086
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_speakerphone:I = 0x1080087

.field public static final stat_sys_tether_bluetooth:I = 0x10806c6

.field public static final stat_sys_tether_general:I = 0x10806c7

.field public static final stat_sys_tether_usb:I = 0x10806c8

.field public static final stat_sys_tether_wifi:I = 0x10806c9

.field public static final stat_sys_throttled:I = 0x10806ca

.field public static final stat_sys_upload:I = 0x1080088

.field public static final stat_sys_upload_done:I = 0x1080089

.field public static final stat_sys_vp_phone_call:I = 0x10800a7
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_vp_phone_call_on_hold:I = 0x10800a8
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public static final stat_sys_warning:I = 0x108008a

.field public static final status_bar_background:I = 0x10806d1

.field public static final status_bar_item_app_background:I = 0x108008b

.field public static final status_bar_item_background:I = 0x108008c

.field public static final sym_action_call:I = 0x108008d

.field public static final sym_action_chat:I = 0x108008e

.field public static final sym_action_email:I = 0x108008f

.field public static final sym_app_on_sd_unavailable_icon:I = 0x10806f1

.field public static final sym_call_incoming:I = 0x1080090

.field public static final sym_call_missed:I = 0x1080091

.field public static final sym_call_outgoing:I = 0x1080092

.field public static final sym_contact_card:I = 0x1080094

.field public static final sym_def_app_icon:I = 0x1080093

.field public static final sym_keyboard_return_holo:I = 0x1080709

.field public static final sym_keyboard_shift:I = 0x108070a

.field public static final sym_keyboard_shift_locked:I = 0x108070b

.field public static final tab_bottom_left:I = 0x108070e

.field public static final tab_bottom_left_v4:I = 0x108070f

.field public static final tab_bottom_right:I = 0x1080710

.field public static final tab_bottom_right_v4:I = 0x1080711

.field public static final tab_indicator_v4:I = 0x108071a

.field public static final text_edit_paste_window:I = 0x1080731

.field public static final text_edit_side_paste_window:I = 0x1080732

.field public static final text_select_handle_left:I = 0x1080734

.field public static final text_select_handle_middle:I = 0x1080737

.field public static final text_select_handle_right:I = 0x108073a

.field public static final textfield_default_mtrl_alpha_op:I = 0x1080749

.field public static final title_bar:I = 0x1080095

.field public static final title_bar_medium:I = 0x1080777

.field public static final title_bar_tall:I = 0x10800a6

.field public static final toast_frame:I = 0x1080096

.field public static final uc_btn_check_material_anim:I = 0x108077b

.field public static final uc_btn_check_show:I = 0x108077c

.field public static final uc_btn_radio_material_anim:I = 0x108077d

.field public static final uc_power_off:I = 0x1080797

.field public static final uc_progressbar_loading:I = 0x1080798

.field public static final uc_scrubber_primary_mtrl_alpha:I = 0x1080799

.field public static final uc_scrubber_progress_horizontal_material:I = 0x108079a

.field public static final uc_scrubber_track_mtrl_alpha:I = 0x108079b

.field public static final uc_seekbar_background_borderless_material:I = 0x108079c

.field public static final uc_switch_thumb_material_anim:I = 0x10807b4

.field public static final uc_switch_track_material:I = 0x10807b5

.field public static final unknown_image:I = 0x10807b7

.field public static final unlock_default:I = 0x10807b8

.field public static final unlock_halo:I = 0x10807b9

.field public static final unlock_ring:I = 0x10807ba

.field public static final unlock_wave:I = 0x10807bb

.field public static final usb_android:I = 0x10807bc

.field public static final usb_android_connected:I = 0x10807bd

.field public static final vpn_connected:I = 0x10807c3

.field public static final vpn_disconnected:I = 0x10807c4

.field public static final zoom_plate:I = 0x1080097


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15124
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
